package Factory;

public class Animal implements NPC {

	public Animal(NPCType type) {
		// TODO Auto-generated constructor stub
		System.out.println("Animal created of type"+type);
	}

	public void fly(){
		System.out.println("Animal flying");
	}

	@Override
	public void walk() {
		System.out.println("Animal walking");
	}

	@Override
	public void running() {
		System.out.println("Animal Running");

	}
}
