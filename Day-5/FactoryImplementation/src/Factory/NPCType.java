package Factory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum NPCType {
	HUMAN_VILLAGER,
	HUMAN_SOLDIER,
	ANIMAL_HORSE,
	ANIMAL_CATTLE,
	ANIMAL_BIRD;

//	private static final List<NPCType> chaosMembers = Collections.unmodifiableList(Arrays.asList(NPCType.values()));
//
//	private static final Random RANDOM = new Random();
//	private static final int SIZE = chaosMembers.size();
// 	public NPCType randomChaos(){
//      return chaosMembers.get(RANDOM.nextInt(SIZE));
//    }

	public static NPCType generateRandomNPC(){
		List<NPCType> chaosMembers = Collections.unmodifiableList(Arrays.asList(NPCType.values()));
		int len = chaosMembers.size();
		int randIndex = new Random().nextInt(len);
		return chaosMembers.get(randIndex);
	}

}


