package Factory;

public class Human implements NPC {
		
	NPCType type;
	public Human(NPCType type2) {
		// TODO Auto-generated constructor stub
		System.out.println("Human created of type"+type2);
	}
	public void swim(){
		System.out.println("Human Swimming");
	}
	@Override
	public void walk() {
		System.out.println("Human Walking...");
	}

	@Override
	public void running() {
		System.out.println("Human running...");

	}
}
