package Factory;
import java.util.*;
import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PanicFactory {
    public PanicFactory() {
        System.out.println("In panic factory");
    }
//    //private static final
//    List<NPCType> chaosMembers = Collections.unmodifiableList(Arrays.asList(NPCType.values()));
//    public List<NPCType> getChaosMembers() {
//        return chaosMembers;
//    }
//    private static final Random RANDOM = new Random();
//    //private static
//    final int SIZE = chaosMembers.size();
//    public NPCType randomChaos(){
//        return chaosMembers.get(RANDOM.nextInt(SIZE));
//    }
    List<NPC> choasArea = new ArrayList<>();
    public List<NPC> createChaos(){
        final int size = 10;
        for(int i=0;i<size;i++){
            choasArea.add(new NPCFactory().createNPC(NPCType.generateRandomNPC()));
        }
        return choasArea;
    }



}
