package Factory;

import java.util.ArrayList;
import java.util.List;

public class Client {
	public static void main(String[] args) {
		NPCFactory npcfactory = new NPCFactory();
		NPC obj = npcfactory.createNPC(NPCType.ANIMAL_HORSE);
		obj.walk();
		NPC obj2 = npcfactory.createNPC(NPCType.HUMAN_VILLAGER);
		obj2.running();
		NPC obj3 = npcfactory.createNPC(NPCType.ANIMAL_CATTLE);
		obj3.walk();

		List<NPC> choasObj = new ArrayList<>();
		PanicFactory panic = new PanicFactory();
		choasObj = panic.createChaos();
		for (NPC object : choasObj){
			object.running();
		}


		//System.out.println(choasObj);

	}
}


//NPC class---> NPC factory(person/animal)

//enum NPCType = villager_male, villager_female, soldier_sword, solder_spy
//enum NPCMmobility = swim, walk, run, fly
