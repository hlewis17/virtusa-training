package Factory;

public class NPCFactory {
		public NPC createNPC(NPCType type ) {
			NPC character = null;
			if(type == NPCType.ANIMAL_BIRD) {
				character = new Animal(type);
			}
			else if(type == NPCType.ANIMAL_HORSE) {
				character = new Animal(type);
			}
			else if(type == NPCType.ANIMAL_CATTLE) {
				character = new Animal(type);
			}
			else if(type == NPCType.HUMAN_VILLAGER) {
				character = new Human(type);
			}
			else if(type == NPCType.HUMAN_SOLDIER) {
				character = new Human(type);
			}
			return character;
		}
}
