package SingleResposibility;

public class Book {
    String title;
    String author;

    String getTitle() {
        return title;
    }
    void setTitle(String title) {
        this.title = title;
    }
    String getAuthor() {
        return author;
    }
    void setAuthor(String author) {
        this.author = author;
    }
//    void searchBook() {
//
//    }
}
//the above code violates the Single
// Responsibility Principle, as the Book class has
// two responsibilities. So we can rewrite this by
// removing the searchBook() method and adding it to a new classs



