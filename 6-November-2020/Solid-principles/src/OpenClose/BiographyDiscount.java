package OpenClose;

//public class BiographyDiscount {
//    String getBiographyDiscount() {
//
//        String discount = "50% on the subject's birthday.";
//
//        return discount;
//
//    }
//}

//After adding the interface it will now be written as


class BiographyDiscount implements BookDiscount {

    @Override
    public String getBookDiscount() {
        String discount = "50% on the subject's birthday.";

        return discount;
    }

}