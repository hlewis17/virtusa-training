package OpenClose;
/* Now, the store wants to hand out cookbooks at a discount price before Christmas.
 We already follow the Single Responsibility Principle, so we create two separate classes:
  CookbookDiscount to hold the details of the discount and DiscountManager to apply the
   discount to the price.
 */
//public class CookBookDiscount {
//    String getCookbookDiscount() {
//
//        String discount = "30% between Dec 1 and 24.";
//
//        return discount;
//    }
//}

//After adding the interface it will now be written as

class CookbookDiscount implements BookDiscount {

    @Override
    public String getBookDiscount() {
        String discount = "30% between Dec 1 and 24.";

        return discount;
    }

}