package OpenClose;

//public class DiscountManager {
//    void processCookbookDiscount(CookBookDiscount discount) {
//
//    }
//    void processBiographyDiscount(BiographyDiscount discount) {
//
//    }
//
//
//}
/*This code works fine until the store management informs us that
their cookbook discount sales were so successful that they want to
 extend it. Now, they want to hand out every biography with a 50% discount
  on the subject’s birthday. To add the new feature,
   we create a new BiographyDiscount class:
 */

/*choose to refactor our code by adding an extra layer of abstraction
 that represents all types of discounts. So, let’s create a new interface
 called BookDiscount that the CookbookDiscount and BiographyDiscount classes will implement.
 */

//after adding interface this can now be written as
class DiscountManager {

    void processBookDiscount(BookDiscount discount) {

    }
}

