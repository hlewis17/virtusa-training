package InterfaceSegregation;

public class AudiobookUI implements AudioAction{
    @Override
    public void seeReviews() {}

    @Override
    public void listenSample() {}
}
