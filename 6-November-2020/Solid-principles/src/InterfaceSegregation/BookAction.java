package InterfaceSegregation;

public interface BookAction {
    void seeReviews();
}
