package LiskovSubstitution;
/* the store also sells fancy hardcovers
 they only want to deliver to their high street shops. So, we
 create a new HardcoverDelivery subclass that extends BookDelivery
 and overrides the getDeliveryLocations() method with its own functionality:
 */
//public class HardCoverDelivery extends BookDelivery {
//    @Override
//    void getDeliveryLocations() {
//
//    }
//}
/*we couldn’t replace the BookDelivery superclass with the AudiobookDelivery subclass without breaking the application.

To solve the problem, we need to fix the inheritance hierarchy. Let’s introduce an extra layer that better differentiates
 book delivery types. The new OfflineDelivery and OnlineDelivery classes
  split up the BookDelivery superclass. We also move the getDeliveryLocations()
   method to OfflineDelivery and create a new getSoftwareOptions() method
   for the OnlineDelivery class (as this is more suitable for online
    deliveries).
 */
class HardcoverDelivery extends OfflineDelivery {

    @Override
    void getDeliveryLocations() {

    }

}

