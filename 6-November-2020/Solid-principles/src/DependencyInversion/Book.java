package DependencyInversion;

public class Book implements Product {
    @Override
    public void seeReviews() {

    }

    @Override
    public void getSample() {

    }
}
