package DependencyInversion;

import SingleResposibility.Book;

public class Shelf {
    Book book;

    void addBook(Book book) {}
    void customizeShelf() {

    }
}
/*Everything looks fine, but as the high-level Shelf class
depends on the low-level Book, the above code violates the
 Dependency Inversion Principle. This becomes clear when the
  store asks us to enable customers to add DVDs to their
  shelves, too. To fulfill the demand, we create
  a new DVD class:
 */

/*The solution is to create an abstraction layer for the
lower-level classes (Book and DVD). We’ll do so by introducing
 the Product interface that both classes will implement.
 */

