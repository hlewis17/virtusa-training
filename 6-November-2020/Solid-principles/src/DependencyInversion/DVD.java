package DependencyInversion;

class DVD implements Product{

    @Override
    public void seeReviews() {

    }

    @Override
    public void getSample() {

    }
}
