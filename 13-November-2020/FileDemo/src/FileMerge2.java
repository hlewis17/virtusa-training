import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileMerge2 {

    public static void main(String[] args) throws IOException {

        List<String> fileNames = new ArrayList<>();
        fileNames.add("file1.txt");
        fileNames.add("file2.txt");
        List<String> content = null;
        Files.createFile(Paths.get("file4.txt"));
        for(String filename:fileNames){
            Stream<String> bands = Files.lines(Paths.get(filename));
             content =  bands.map(String::toUpperCase).collect(Collectors.toList());

            Files.write(Paths.get("file4.txt"),content
                    , StandardOpenOption.APPEND);
            bands.close();
            }
    }
}

