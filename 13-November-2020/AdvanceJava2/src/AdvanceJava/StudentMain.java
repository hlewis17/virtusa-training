package AdvanceJava;

import java.util.*;

public class StudentMain {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        Map<String, Integer> marks1 = new HashMap<>();
        marks1.put("SCIENCE", 90);
        marks1.put("LANGUAGE", 85);
        marks1.put("HISTORY", 80);
        marks1.put("MATHEMATICS", 75);
        Student s1 = new Student("1", marks1);
        String ss1 = s1.toString();


        Map<String, Integer> marks2 = new HashMap<>();
        marks2.put("SCIENCE", 90);
        marks2.put("LANGUAGE", 80);
        marks2.put("HISTORY", 70);
        marks2.put("MATHEMATICS", 95);
        Student s2 = new Student("2", marks2);
        s2.toString();

        studentList.add(s1);
        studentList.add(s2);
        Map<Subject, List<Student>> highScores = new HashMap<>();
        CalculateBest ob = new CalculateBest();
        highScores = ob.getBestScores(studentList);


        Map<Subject,List<Student>> finalHighScores = getBestScores(studentList);


        for (Map.Entry<Subject,List<Student>> e : finalHighScores.entrySet())
            System.out.println(e.getKey() + " " + e.getValue());


    }
        public static Map<Subject, List<Student>> getBestScores(List<Student> studentList) {
            Map<Subject, List<Student>> highScores = new HashMap<>();

            for(Subject sub:Subject.values()){
                highScores.put(sub, getTopScoreForSub(sub.name(),studentList));
            }
            return highScores;
        }
        public static List<Student> getTopScoreForSub(String sub, List<Student> studentList){
            List<Student> subWiseMax = new ArrayList<>();
            Integer max = 0;
//            List<Student> temp = new ArrayList<>();
            Student temp = null;
            for (Student student : studentList)
            {
                Integer mark = student.report.get(sub);
                if(mark > max){
                    max = mark;
                    temp = student;
                }
                else if(mark == max){
                    if(Integer.parseInt(student.id) < Integer.parseInt(temp.id)) temp = student;
                }

            }
            subWiseMax.add(temp);
            return subWiseMax;
        }

    }

