package AdvanceJava;

import java.util.Map;

public class Student {
    String id;
    Map<String, Integer> report;

    public Student(String id, Map<String, Integer> report){
        this.id = id;
        this.report = report;
    }

    @Override
    public String toString(){
        return "Student{" + "id='" + id + '}';
    }

}
enum Subject {
    SCIENCE,
    MATHEMATICS,
    LANGUAGE,
    HISTORY
}
