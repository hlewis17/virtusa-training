package AdvanceJava;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMain {
    public static void main(String[] args){
        Test ob = new Test();
        //testing for exercise 1
        List<Integer> integerList = new ArrayList<>();
        integerList.add(22);
        integerList.add(212);
        integerList.add(121);
        double integerListAverage = ob.calculateAverage(integerList);
        System.out.println("Exercise 1 tested: " + integerListAverage);

        //testing for exercise 2
        List<ArrayList<String>> listOfLists = new ArrayList<ArrayList<String>>();
        ArrayList<String> list1  = new ArrayList<>();
        list1.add("name");
        list1.add("place");
        listOfLists.add(list1);
        ArrayList<String> list2  = new ArrayList<>();
        list1.add("country");
        list1.add("phone");
        listOfLists.add(list2);
        List<String> resultList = new ArrayList<>();
        resultList = ob.getFlattenList(listOfLists);
        System.out.println("Exercise 2 tested: " +resultList);

        //test for exercise 3
        Map<String,String> map1 = new HashMap<>();
        map1.put("name","hashika");
        map1.put("country", "USA");
        map1.put("company","virtusa");
        System.out.println("Exercise 3 tested:");
        ob.printMapKeyValue(map1);

        //test for exercise 4
        String response;
        System.out.println("Exercise 4 tested:");
        response = ob.findValue(map1,"name");
        System.out.println(response);
        String response2;
        response2 = ob.findValue(map1,"loc");
        System.out.println(response2);



        //test for exercise5
        List<String> listOfStrings = new ArrayList<>();
        listOfStrings.add("hello");
        listOfStrings.add("world");
        listOfStrings.add("Hi");
        listOfStrings.add("Bye");
        Map<Integer,List<String>> map = new HashMap<Integer, List<String>>();
        map = ob.createMap(listOfStrings);
        System.out.println("Exercise 5 tested: " +map);






    }
}
