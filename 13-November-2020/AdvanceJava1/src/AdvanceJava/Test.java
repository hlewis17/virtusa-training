package AdvanceJava;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Test {

    //1
    public double calculateAverage(List<Integer> l1){
        Integer sum = 0;
        if(l1.isEmpty() != true){
            for(Integer num :l1){
                sum += num;
            }
        }
        return sum.doubleValue()/l1.size();
    }

    public List<String> getFlattenList(List<ArrayList<String>> listOfListOfString){
        List<String> flat = listOfListOfString.stream().flatMap(List :: stream).collect(Collectors.toList());
        return flat;
    }

    public void printMapKeyValue(Map<String, String> map){
        for(Map.Entry<String,String> entry:map.entrySet()){
            System.out.println("KEY: "+entry.getKey() + "= "+"VALUE: "+entry.getValue());
        }
    }

    public String findValue(Map<String, String> map,  String searchKey){
        if(map.get(searchKey) != null) return map.get(searchKey);
        else return "STRING NOT FOUND";
    }

    public Map<Integer,List<String>> createMap(List<String> givenlist){
        Map<Integer,List<String>> map = new HashMap<>();
        for (String str:givenlist){
            if(!map.containsKey(str.length())){
                map.put(str.length(),new ArrayList<>());
            }
            map.get(str.length()).add(str);
        }
        return map;
    }
}

