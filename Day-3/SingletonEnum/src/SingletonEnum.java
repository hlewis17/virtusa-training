public enum SingletonEnum {
 INSTANCE;
 int i;
 public void show(){
  System.out.println(i);
 }

}
//works from 1.5v onwards
//enum will have a private constructor. it is a special type of class
//even if we are working with locking or deserialization which uses readObject()
//that creates a new object every single time, when using enum, we use readResolve()
//it will not create new object but current version.