public class SIngletonClient {
    public static void main(String[] args){
        SingletonEnum obj1 = SingletonEnum.INSTANCE;
        SingletonEnum obj2 = SingletonEnum.INSTANCE;

        System.out.println(obj1 == obj2);
        System.out.println(obj1.hashCode());
        System.out.println(obj2.hashCode());

        obj1.i = 5;
        obj1.show();

        obj2.i = 6;
        obj2.show();
        obj1.show();
    }
}
